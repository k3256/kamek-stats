export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.config.errorHandler = function (err, vm, info) {
    console.log(`Error: ${err.toString()}\nInfo: ${info}`);
  };
});
