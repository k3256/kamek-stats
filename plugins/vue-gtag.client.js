import { defineNuxtPlugin } from "#app";
import VueGtag from "vue-gtag";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(
    VueGtag,
    {
      config: {
        id: nuxtApp.$config.googleAnalytics.id,
      },
      appName: "Kamek Book",
      pageTrackerScreenviewEnabled: true,
    },
    nuxtApp.vueApp.router
  );
});
