import { defineNuxtPlugin } from "#app";
import PrimeVue from "primevue/config";
import Button from "primevue/button";
import InputText from "primevue/inputtext";
import InputNumber from "primevue/inputnumber";
import InputSwitch from "primevue/inputswitch";
import Textarea from "primevue/textarea";
import Toast from "primevue/toast";
import ToastService from "primevue/toastservice";
import InputMask from "primevue/inputmask";

import Dialog from "primevue/dialog";
import Password from "primevue/password";
import Divider from "primevue/divider";
import Calendar from "primevue/calendar";
import Dropdown from "primevue/dropdown";
import Checkbox from "primevue/checkbox";
import Image from "primevue/image";
import Card from "primevue/card";

import ProgressSpinner from "primevue/progressspinner";
import Ripple from "primevue/ripple";
import BadgeDirective from "primevue/badgedirective";
import StyleClass from "primevue/styleclass";
import Tooltip from "primevue/tooltip";

import DataTable from "primevue/datatable";
import Column from "primevue/column";
import Message from "primevue/message";
import FileUpload from "primevue/fileupload";

import TabView from "primevue/tabview";
import TabPanel from "primevue/tabpanel";
import TabMenu from "primevue/tabmenu";

import ProgressBar from "primevue/progressbar";
import Rating from "primevue/rating";

import ScrollPanel from "primevue/scrollpanel";
import Sidebar from "primevue/sidebar";
import Chart from "primevue/chart";
import OverlayPanel from "primevue/overlaypanel";
import Menu from "primevue/menu";
import TieredMenu from "primevue/tieredmenu";
import SelectButton from "primevue/selectbutton";
import Accordion from "primevue/accordion";
import AccordionTab from "primevue/accordiontab";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(PrimeVue, { ripple: true });
  nuxtApp.vueApp.use(ToastService);

  nuxtApp.vueApp.component("Button", Button);
  nuxtApp.vueApp.component("InputText", InputText);
  nuxtApp.vueApp.component("InputNumber", InputNumber);
  nuxtApp.vueApp.component("Textarea", Textarea);
  nuxtApp.vueApp.component("InputSwitch", InputSwitch);
  nuxtApp.vueApp.component("InputMask", InputMask);
  nuxtApp.vueApp.component("Toast", Toast);

  nuxtApp.vueApp.component("Dialog", Dialog);
  nuxtApp.vueApp.component("Password", Password);
  nuxtApp.vueApp.component("Divider", Divider);
  nuxtApp.vueApp.component("Calendar", Calendar);
  nuxtApp.vueApp.component("Dropdown", Dropdown);
  nuxtApp.vueApp.component("Checkbox", Checkbox);
  nuxtApp.vueApp.component("Image", Image);
  nuxtApp.vueApp.component("Card", Card);
  nuxtApp.vueApp.component("FileUpload", FileUpload);

  nuxtApp.vueApp.component("ProgressSpinner", ProgressSpinner);

  nuxtApp.vueApp.directive("ripple", Ripple);
  nuxtApp.vueApp.directive("badge", BadgeDirective);
  nuxtApp.vueApp.directive("tooltip", Tooltip);
  nuxtApp.vueApp.directive("styleclass", StyleClass);

  nuxtApp.vueApp.component("DataTable", DataTable);
  nuxtApp.vueApp.component("Column", Column);
  nuxtApp.vueApp.component("Message", Message);

  nuxtApp.vueApp.component("TabView", TabView);
  nuxtApp.vueApp.component("TabPanel", TabPanel);
  nuxtApp.vueApp.component("TabMenu", TabMenu);

  nuxtApp.vueApp.component("ProgressBar", ProgressBar);
  nuxtApp.vueApp.component("Rating", Rating);

  nuxtApp.vueApp.component("ScrollPanel", ScrollPanel);
  nuxtApp.vueApp.component("Sidebar", Sidebar);
  nuxtApp.vueApp.component("Chart", Chart);
  nuxtApp.vueApp.component("OverlayPanel", OverlayPanel);
  nuxtApp.vueApp.component("Menu", Menu);
  nuxtApp.vueApp.component("TieredMenu", TieredMenu);
  nuxtApp.vueApp.component("SelectButton", SelectButton);
  nuxtApp.vueApp.component("Accordion", Accordion);
  nuxtApp.vueApp.component("AccordionTab", AccordionTab);
});
