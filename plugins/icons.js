import IconStable from "~icons/mdi/arrow-right-thick";
import IconDown from "~icons/mdi/arrow-down-right-thick";
import IconUp from "~icons/mdi/arrow-up-right-thick";
import IconLeft from "~icons/mdi/arrow-left-thick";
import IconRight from "~icons/mdi/arrow-right-thick";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component("IconUp", IconUp);
  nuxtApp.vueApp.component("IconLeft", IconLeft);
  nuxtApp.vueApp.component("IconRight", IconRight);
  nuxtApp.vueApp.component("IconStable", IconStable);
  nuxtApp.vueApp.component("IconDown", IconDown);
});
