import { useI18n } from "vue-i18n";

export default defineNuxtPlugin((nuxtApp) => {
  const getItemLLabel = (type, key, params, { noFirstUpper, t } = {}) => {
    if (!t) {
      const i18n = useI18n();
      t = i18n.t;
    }
    const pts = {
      types: t("items.types." + type),
      type: t("items.type." + type),
      atype: t("items.atype." + type),
      thistype: t("items.thistype." + type),
      oftype: t("items.oftype." + type),
      oftypes: t("items.oftypes." + type),
    };
    let ress = t("items." + key, { ...pts, ...params });
    if (!noFirstUpper) {
      ress = ress.substr(0, 1).toUpperCase() + ress.substr(1);
    }
    return ress;
  };
  return {
    provide: {
      itt: getItemLLabel,
    },
  };
});
