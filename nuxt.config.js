import { defineNuxtConfig } from "nuxt";
import { buildRouteNames } from "./helpers/BuildRoutesNames";

const routes = buildRouteNames();
// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
  css: [
    "primevue/resources/themes/lara-dark-indigo/theme.css",
    "primevue/resources/primevue.min.css",
    "primeicons/primeicons.css",
    "primeflex/primeflex.min.css",
    "@/assets/css/main.css",
  ],
  build: {
    transpile: ["primevue"],
  },
  modules: ["@funken-studio/sitemap-nuxt-3"],
  sitemap: {
    hostname: "https://kamekbook.com",
    path: "sitemap.xml",
    cacheTime: 1,
    routes: routes,
    defaults: {
      changefreq: "daily",
      priority: 1,
      lastmod: new Date().toISOString(),
    },
  },
  buildModules: ["@nuxtjs/i18n", "unplugin-icons/nuxt"],
  publicRuntimeConfig: {
    googleAnalytics: {
      id: "G-1LTWK7B3BC",
    },
  },
  i18n: {
    locales: [
      {
        code: "fr",
        iso: "fr-FR",
        file: "fr.json",
        name: "Français",
        countryFlag: "fr",
      },
      {
        code: "en",
        iso: "en-US",
        file: "en.json",
        name: "English",
        countryFlag: "gb",
      },
      {
        code: "de",
        iso: "de-DE",
        file: "en.json",
        notTranslated: true,
        name: "Deutsh",
        countryFlag: "de",
      },
      {
        code: "es",
        iso: "es-ES",
        file: "en.json",
        notTranslated: true,
        name: "Español",
        countryFlag: "es",
      },
      {
        code: "zh",
        iso: "zh-CN",
        file: "en.json",
        notTranslated: true,
        name: "Chinese",
        countryFlag: "cn",
      },
      {
        code: "it",
        iso: "it-IT",
        file: "en.json",
        notTranslated: true,
        name: "Italiano",
        countryFlag: "it",
      },
      {
        code: "ja",
        iso: "ja-JA",
        file: "en.json",
        notTranslated: true,
        name: "Japanese",
        countryFlag: "jp",
      },
      {
        code: "ko",
        iso: "ko-KO",
        file: "en.json",
        notTranslated: true,
        name: "Korean",
        countryFlag: "kr",
      },
      {
        code: "zh-TW",
        iso: "zh-TW",
        file: "en.json",
        notTranslated: true,
        name: "Taiwanese",
        countryFlag: "tw",
      },
      {
        code: "pt",
        iso: "pt-PT",
        file: "en.json",
        notTranslated: true,
        name: "Portguese",
        countryFlag: "pt",
      },
    ],
    defaultLocale: "en",
    detectBrowserLanguage: {
      fallbackLocale: "en",
    },
    strategy: "no_prefix",
    lazy: { skipNuxtState: true },
    langDir: "locales",
    vueI18n: {
      legacy: false,
      fallbackLocale: "en",
    },
  },
  ssr: false,
  nitro: {
    prerender: {
      routes: routes,
    },
  },
});
