export const bonusPoints = (collection, collect) => {
  if (collect) {
    let p = collect?.p;
    if (collect?.mii) {
      p += collection.nbMiiPoints;
    }
    return (2 * (p * (collect.l - 1) * 150)) / 30000;
  }
  return 0;
};

/**
 * a and b are objects containing collect (l, p, c) and fromLevel to prevent some objects from popping up
 * preferredId will pop first if mentionned
 * @param {*} a
 * @param {*} b
 * @returns
 */
export const sortItems =
  (
    collection,
    collect = (it) => it?.collect,
    fromLevel = (it) => it?.fromLevel,
    preferredId = null
  ) =>
  (a, b) => {
    let rvalue = 0;
    let collect_a = collect(a),
      collect_b = collect(b),
      fl_a = fromLevel(a),
      fl_b = fromLevel(b);
    let a_available = collect_a && (!fl_a || fl_a <= collect_a.l),
      b_available = collect_b && (!fl_b || fl_b <= collect_b.l);

    if (!a_available && !b_available) {
      if (collect_a == null && collect_b != null) {
        rvalue = 1;
      } else if (collect_a != null && collect_b == null) {
        rvalue = -1;
      } else {
        rvalue = (fl_a || 0) - (fl_b || 0);
      }
    } else if (!a_available && b_available) {
      rvalue = 1;
    } else if (a_available && !b_available) {
      rvalue = -1;
    } else {
      //if (collect_a?.l === collect_b?.l) {
      /*let a_points = collect_a?.p,
        b_points = collect_b?.p;
      if (collect_a?.mii) {
        a_points += collection.nbMiiPoints;
      }
      if (collect_b?.mii) {
        b_points += collection.nbMiiPoints;
      }
      rvalue = b_points - a_points;
    } else {
      rvalue = collect_b?.l - collect_a?.l;
      */
      let a_bonus = bonusPoints(collection, collect_a),
        b_bonus = bonusPoints(collection, collect_b);
      rvalue = b_bonus - a_bonus;
    }
    if (preferredId && rvalue === 0) {
      if (a?.id === preferredId) {
        rvalue = -1;
      } else if (b?.id === preferredId) {
        rvalue = 1;
      }
    }
    return rvalue;
  };
