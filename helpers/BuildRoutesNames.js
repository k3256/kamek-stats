import { useDb } from "../composables/useDb.js";

export const buildRouteNames = () => {
  const { db, getItemSlug, getCourseSlug, getTourSlug, slugify } = useDb();

  return [
    "/",
    "/settings/",
    "/getstarted",
    "/drivers/",
    ...db.drivers.list.map((d) => "/drivers/" + getItemSlug("drivers", d)),
    "/karts/",
    ...db.karts.list.map((k) => "/karts/" + getItemSlug("karts", k)),
    "/gliders/",
    ...db.gliders.list.map((g) => "/gliders/" + getItemSlug("gliders", g)),
    "/courses/",
    ...db.preferred.list.map((c) => "/courses/" + getCourseSlug(c)),
    "/tours/",
    ...db.cups.list.map((t) => "/tours/" + getTourSlug(t)),
    ...db.cups.list.flatMap((t) => {
      return t.c
        .map((c) => "/tours/" + getTourSlug(t) + "/" + slugify(c.n))
        .concat(
          ...["most_used_drivers", "most_used_karts", "most_used_gliders"].map(
            (k) => "/tours/" + getTourSlug(t) + "/" + k
          )
        );
    }),
  ];
};
