export const generalGrades = [
  { name: "cups" },
  { name: "rankedCups" },
  { name: "favorites" },
  { name: "favorites_nonunique" },
  { name: "oldness" },
  { name: "skill" },
  { name: "total", total: true },
];

export const collectionGrades = [
  { name: "effort10" },
  { name: "effort50p" },
  /* { name: "concurrency" },*/
  { name: "wanted", bonus: true },
  { name: "wantedhcity", bonus: true },
  { name: "utility", bonus: true },
  { name: "utilityhcity", bonus: true },
  { name: "total", total: true },
];
export const initStat = () => {
  return {
    nb: 0,
    total: 0,
    min: Infinity,
    max: 0,
  };
};
export const increaseStat = (s, key, val) => {
  s[key].nb++;
  s[key].total += val;
  if (val < s[key].min && val > 0) s[key].min = val;
  if (val > s[key].max && val < Infinity) s[key].max = val;
};
export const computeRegression = (val, objMinMax) => {
  return (val - objMinMax.min) / (objMinMax.max - objMinMax.min);
};
export const getGrade01 = (grade, precision = 0) => {
  if (isNaN(grade)) return 0;
  let res = Number(grade * 100).toFixed(precision);
  if (res > 100) return Number(100).toFixed(precision);
  return res;
};
