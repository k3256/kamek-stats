import { reactive, computed } from "vue";
import { useParams } from "./useParams.js";
import {
  initStat,
  increaseStat,
  computeRegression,
} from "../helpers/GradesHelper.js";

import dbfull from "../assets/db.json"; // assert { type: "json" };

let db;

const { params, loadParams } = useParams();

const getDriverObjectName = (id) => {
  let o = db.drivers.skills.find((s) => s.id === id);
  return o?.name;
};
const getDriverObjectsList = () => {
  return db.drivers.skills.map(({ id, n }) => ({ label: n, value: id }));
};
const getGliderObjectName = (id) => {
  let o = db.gliders.skills.find((s) => s.id === id);
  return o?.name;
};
const getGliderObjectsList = () => {
  return db.gliders.skills.map(({ id, n }) => ({ label: n, value: id }));
};
const getDriverById = (id) => {
  let o = db.drivers.list.find((s) => s.id === id);
  return o;
};
const slugify = (name) => {
  return name
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .toLowerCase()
    .replace(/[^a-z0-9]/g, " ")
    .replace(/  /g, " ")
    .trim()
    .replace(/ /g, "-");
};
const getDriverBySlug = (name) => {
  let o = db.drivers.list.find((s) => slugify(s.n) === name);
  return o;
};
const getItemSlug = (type, object) => {
  return slugify(object.n);
};
const getKartById = (id) => {
  let o = db.karts.list.find((s) => s.id === id);
  return o;
};
const getKartBySlug = (name) => {
  let o = db.karts.list.find((s) => slugify(s.n) === name);
  return o;
};
const getGliderById = (id) => {
  let o = db.gliders.list.find((s) => s.id === id);
  return o;
};
const getGliderBySlug = (name) => {
  let o = db.gliders.list.find((s) => slugify(s.n) === name);
  return o;
};
const getCourseBySlug = (name) => {
  let o = db.stats_courses.find(
    (s) =>
      slugify((s.c ? s.c + "_" : "") + s.n + (s.t ? "_" + s.t : "")) === name
  );
  return o;
};
const getTourBySlug = (name) => {
  let o = db.cups.list.find((s) => slugify(s.n) === name);
  return o;
};
const getCourseSlug = (s) => {
  return slugify((s.c ? s.c + "_" : "") + s.n + (s.t ? "_" + s.t : ""));
};
const getTourSlug = (s) => {
  return slugify(s.n);
};
const getItemById = (type, id) => {
  if (type === "drivers") {
    return getDriverById(id);
  } else if (type === "karts") {
    return getKartById(id);
  } else if (type === "gliders") {
    return getGliderById(id);
  }
};
const getObjectKey = (type) => {
  if (type === "drivers") {
    return "d";
  } else if (type === "karts") {
    return "k";
  } else if (type === "gliders") {
    return "g";
  }
};
const getCoursesForItem = (type, id) => {
  let d = getItemById(type, id),
    key = getObjectKey(type);
  if (!d) return null;
  if (d && d.preferred_courses) return d.preferred_courses;
  let res = { preferred: [], second_preferred: [] };
  for (let c of db.preferred.list) {
    if (c.p[key].includes(id)) res.preferred.push(c);
    if (c.p2[key].includes(id)) res.second_preferred.push(c);
    let conditionnedCourses = c.p2[key].filter(Array.isArray);
    if (conditionnedCourses.length > 0) {
      for (let cond of conditionnedCourses) {
        if (cond[0] === id) {
          res.preferred.push({ ...c, fromLevel: cond[1] });
          res.second_preferred.push(c);
        }
      }
    }
  }
  res.preferred.sort((a, b) => (a.fromLevel || 0) - (b.fromLevel || 0));
  d.preferred_courses = res;
  return res;
};
const getTour = (id) => {
  let o = db.cups.list.find((s) => s.id == id);
  return o;
};
const getCourseById = (id) => {
  let o = db.preferred.list.find((s) => s.id === id);
  return o;
};
const getCupStatForItem = (type, id) => {
  let d = getItemById(type, id),
    key = getObjectKey(type);
  if (!d) return null;
  if (d && d.cups_stats) return d.cups_stats;
  if (!d.preferred_courses) getCoursesForItem(type, id);
  let res = [];
  let prefids = d.preferred_courses.preferred.map((c) => c.id);
  let sprefids = d.preferred_courses.second_preferred.map((c) => c.id);
  for (let c of db.cups.list) {
    let nbOthers = 0, // number of classified courses where driver is top ranked (or ranked up to top) but not w1, w2 or ranked up
      nbW1 = 0, // number of week 1 classified courses where driver is top ranked (or ranked up to top)
      nbW2 = 0, // number of week 2 classified courses where driver is top ranked (or ranked up to top)
      nbRup = 0, // number of courses where driver is ranked up to top
      nbUnique = 0; // number of courses which are unique (appeared one time only in MKT history)
    let i = 0;
    let courselist = [];
    if (d.t <= c.id) {
      for (let cc of c.c) {
        let nbfav = cc.c.reduce((acc, val) => {
          let ispref = prefids.includes(val);
          if (ispref) {
            courselist.push(val);
          }
          return acc + (ispref ? 1 : 0);
        }, 0);
        let nbRuppart = cc.c.reduce((acc, val, idxcourse) => {
          let r = 0; // rank => 1 object
          if (prefids.includes(val)) r = 2; // rank => 3 objects
          else if (sprefids.includes(val)) r = 1; // rank => 2 objects
          let toadd = 0; // will be 1 if driver is ranked up for this course
          if (r < 2) {
            // if already top ranked, do not consider rank up -> course is counted as fav
            if (c.up) {
              // list of upranks for tour
              c.up.forEach((upp) => {
                if (upp[key] && upp[key].includes(id)) {
                  if (upp.ref === "f" && idxcourse === 0)
                    r++; // first course of cup and driver is up for all first course
                  else if (upp.ref === "s" && idxcourse === 1)
                    r++; // second course of cup and driver is up for all second course
                  else if (upp.ref === "t" && idxcourse === 2)
                    r++; // third course of cup and driver is up for all third course
                  else if (upp.ref === i) r++; // driver is up for all courses of this cup
                }
              });
              if (r >= 2) {
                toadd = 1;
              }
            }
          }
          if (toadd === 1) {
            courselist.push(val);
          }
          return acc + toadd;
        }, 0);
        if (cc.rk === 1) nbW1 += nbfav + nbRuppart;
        else if (cc.rk === 2) nbW2 += nbfav + nbRuppart;
        else {
          nbOthers += nbfav;
          nbRup += nbRuppart;
        }
        i++;
      }
    }
    nbUnique = courselist.reduce((acc, val) => {
      let c = getCourseById(val);
      return acc + (c.unique ? 1 : 0);
    }, 0);
    res.push({
      id: c.id,
      name: c.n,
      startDate: c.s,
      endDate: c.e,
      nbOthers,
      nbW1,
      nbW2,
      nbRup,
      nbTotal: nbW1 + nbW2 + nbRup + nbOthers,
      nbUnique,
      courselist,
    });
  }
  d.cups_stats = res;
  return res;
};

const computeTotalStatsForItem = (type, id) => {
  let d = getItemById(type, id),
    key = getObjectKey(type);
  if (!d) return null;

  if (d && d.total_stats) return d.total_stats;
  if (!d.cups_stats) getCupStatForItem(type, id);

  d.total_stats = {
    totalcourses: d.cups_stats.reduce((acc, val) => acc + val.nbTotal, 0),
    totalclassified: d.cups_stats.reduce(
      (acc, val) => acc + val.nbW1 + val.nbW2,
      0
    ),
    lastyeartotal: d.cups_stats.reduce(
      (acc, val, idx) =>
        acc + (d.cups_stats.length - idx <= 26 ? val.nbTotal : 0),
      0
    ),
    lastyearclassified: d.cups_stats.reduce(
      (acc, val, idx) =>
        acc + (d.cups_stats.length - idx <= 26 ? val.nbW1 + val.nbW2 : 0),
      0
    ),
    totalunique: d.cups_stats.reduce((acc, val) => acc + val.nbUnique, 0),
    lastyeartotalunique: d.cups_stats.reduce(
      (acc, val, idx) =>
        acc + (d.cups_stats.length - idx <= 26 ? val.nbUnique : 0),
      0
    ),
  };
  return d.total_stats;
};

let breakpoint = 0;
const easeInOutQuad = (x) => {
  return x < breakpoint ? 2 * x * x : 1 - Math.pow(-2 * x + 2, 2) / 2;
};
//Compute global stats about number of courses, ranked cups, total cups depending on oldness
const computePredispositions = () => {
  let allstats = { drivers: {}, karts: {}, gliders: {} };
  ["drivers", "karts", "gliders"].forEach((t) => {
    allstats[t] = {
      favorites: initStat(),
      favorites_nonunique: initStat(),
      rankedCups: initStat(),
      cups: initStat(),
      items: {},
    };
    for (let item of db[t].list) {
      let item_predis = {
        rankedCups: initStat(),
        cups: initStat(),
      };
      item.cups_stats.forEach((s) => {
        let fromOrigin = s.id - item.t,
          percentreduc = s.id / db.cups.list.length;
        if (fromOrigin >= 0) {
          //ranked cups
          increaseStat(
            item_predis,
            "rankedCups",
            (s.nbW1 + s.nbW2) * easeInOutQuad(percentreduc)
          );
          //cups
          increaseStat(
            item_predis,
            "cups",
            s.nbTotal * easeInOutQuad(percentreduc)
          );
        }
      });
      let oldness = db.cups.list.length - item.t;
      item_predis.rankedCups.total *= 1 - oldness / db.cups.list.length;
      item_predis.cups.total *= 1 - oldness / db.cups.list.length;
      allstats[t].items[item.id] = item_predis;
      increaseStat(allstats[t], "rankedCups", item_predis.rankedCups.total);
      increaseStat(allstats[t], "cups", item_predis.cups.total);
      increaseStat(
        allstats[t],
        "favorites",
        item.preferred_courses.preferred.length
      );
      increaseStat(
        allstats[t],
        "favorites_nonunique",
        item.preferred_courses.preferred.reduce(
          (acc, v) => acc + (v.unique ? 0 : 1),
          0
        )
      );
    }
  });
  return allstats;
};

const computeGeneralGrades = (type, item) => {
  let oldness = db.cups.list.length - item.t;
  item.grades = computed(() => {
    let stats = db.predispo_stats.value[type],
      itemstats = stats.items[item.id];
    let paramSkill = params.skills[type].find((s) => s.id == item.s);
    let res = {
      cups: Math.floor(
        computeRegression(itemstats.cups.total, stats.cups) * 100
      ),
      rankedCups: Math.floor(
        computeRegression(itemstats.rankedCups.total, stats.rankedCups) * 100
      ),
      favorites: Math.floor(
        computeRegression(
          item.preferred_courses.preferred.length,
          stats.favorites
        ) * 100
      ),
      favorites_nonunique: Math.floor(
        computeRegression(
          item.preferred_courses.preferred.reduce(
            (acc, v) => acc + (v.unique ? 0 : 1),
            0
          ),
          stats.favorites_nonunique
        ) * 100
      ),
      oldness: Math.floor((1 - oldness / db.cups.list.length) * 100),
      skill: (paramSkill && paramSkill.grade) || 0,
    };
    let grade = 0,
      weight = 0;
    Object.keys(res).forEach((gkey) => {
      let w = params.grades.general[gkey] || 0;
      grade += res[gkey] * w;
      weight += w;
    });
    grade /= weight;
    res.total = Number(grade).toFixed(1);
    return res;
  });
};

let initialized = false;
const initDb = (force = false) => {
  if (initialized && !force) return;
  initialized = true;
  let timeinit = new Date().getTime();
  // set stats of courses (unicity, number of appearance)
  let courseStatsPerId = [];
  let idtour = 0;
  for (let tour of db.cups.list) {
    for (let cup of tour.c) {
      for (let courseId of cup.c) {
        if (!courseStatsPerId[courseId]) {
          courseStatsPerId[courseId] = {
            nbTimes: 0,
            nbTimesLastYear: 0,
            nbRanked: 0,
            nbRankedLastYear: 0,
            cupslist: new Set(),
            cupsnb: {},
          };
        }
        courseStatsPerId[courseId].nbTimes++;
        if (cup.rk === 1 || cup.rk === 2) {
          courseStatsPerId[courseId].nbRanked++;
        }
        if (db.cups.list.length - idtour <= 26) {
          courseStatsPerId[courseId].nbTimesLastYear++;
          if (cup.rk === 1 || cup.rk === 2) {
            courseStatsPerId[courseId].nbRankedLastYear++;
          }
        }
        courseStatsPerId[courseId].cupslist.add(idtour + 1);
        if (!courseStatsPerId[courseId].cupsnb[idtour + 1])
          courseStatsPerId[courseId].cupsnb[idtour + 1] = {
            w1: 0,
            w2: 0,
            others: 0,
          };
        if (cup.rk === 1) {
          courseStatsPerId[courseId].cupsnb[idtour + 1].w1++;
        } else if (cup.rk === 2) {
          courseStatsPerId[courseId].cupsnb[idtour + 1].w2++;
        } else {
          courseStatsPerId[courseId].cupsnb[idtour + 1].others++;
        }
      }
    }
    idtour++;
  }

  // set it on preffered courses (used to reference items)
  db.preferred.list = db.preferred.list
    .filter((p) => Boolean(p && p.id && courseStatsPerId[p.id]))
    .map((pref) => {
      if (courseStatsPerId[pref.id]) {
        return {
          ...pref,
          stats: courseStatsPerId[pref.id],
          unique: pref.city,
        };
      } else {
        return pref;
      }
    });

  let stats_courses = Object.keys(courseStatsPerId).map((id) => ({
    id,
    ...courseStatsPerId[id],
    ...db.preferred.list.find((c) => c.id == id),
    nbTotal: Object.keys(courseStatsPerId[id].cupsnb).reduce(
      (acc, val) =>
        acc +
        courseStatsPerId[id].cupsnb[val].w1 +
        courseStatsPerId[id].cupsnb[val].w2 +
        courseStatsPerId[id].cupsnb[val].others,
      0
    ),
  }));
  stats_courses.sort((a, b) => b.nbTimes - a.nbTimes);
  db.stats_courses = stats_courses;

  let itemskeys = ["drivers", "karts", "gliders"];
  // set stats of drivers
  for (let k of itemskeys) {
    for (let item of db[k].list) {
      // init driver preffered courses
      getCoursesForItem(k, item.id);
      // init driver courses stats
      getCupStatForItem(k, item.id);
      // aggregate driver stats
      computeTotalStatsForItem(k, item.id);
    }
  }

  // iniit mii
  let nbmiis = 0;
  for (let driver of db.drivers.list) {
    if (driver.n.indexOf("Mii") >= 0) {
      driver.mii = true;
      nbmiis++;
    }
  }
  db.nbMiis = nbmiis;
  console.log("Init database in", new Date().getTime() - timeinit, "ms");
};

const loadDbGrades = () => {
  loadParams();
  db.predispo_stats = computed(() => computePredispositions());

  for (let k of ["drivers", "karts", "gliders"]) {
    for (let item of db[k].list) {
      computeGeneralGrades(k, item);
    }
  }
};

export const useDb = (dbloc, force = false) => {
  if (!dbloc) {
    db = dbfull;
  } else {
    db = dbloc;
  }
  breakpoint = 1 - params.importance / db.cups.list.length;
  initDb(force);
  return {
    db,
    nbTours: db.cups.list.length,
    drivers: db.drivers.list,
    gliders: db.gliders.list,
    karts: db.karts.list,
    getDriverObjectName,
    getDriverObjectsList,
    getGliderObjectName,
    getGliderObjectsList,
    getTour,
    getDriverById,
    getKartById,
    getGliderById,
    getItemById,
    getDriverBySlug,
    getItemSlug,
    getGliderBySlug,
    getKartBySlug,
    getCourseBySlug,
    getCourseById,
    getCourseSlug,
    getTourBySlug,
    getTourSlug,
    slugify,
    loadDbGrades,
  };
};
