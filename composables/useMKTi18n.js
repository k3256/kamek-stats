import { useI18n } from "vue-i18n";
import { watch } from "vue";

const languages = [
  "cn",
  "de",
  "es",
  "fr",
  "it",
  "ja",
  "ko",
  "tw",
  "en",
  "us-es",
  "pt",
];

const languagesMatch = {
  zh: "cn",
  "zh-TW": "tw",
};

const loadedLanguages = {};

const loadLanguage = async (lang) => {
  if (loadedLanguages[lang]) {
    return loadedLanguages[lang];
  } else {
    let trans = await new Promise((resolve, reject) => {
      fetch("/mkt-locales/" + lang + ".json").then(function (response) {
        resolve(response.json());
      });
    });
    loadedLanguages[lang] = trans;
  }
};

const translations = reactive({});

const mktt = (domain, id) => {
  return translations[domain] && translations[domain]["" + id];
};

const initMkti18n = () => {
  const { locale } = useI18n();
  watch(
    () => locale.value,
    async (nl) => {
      let lang = nl;
      if (!lang) lang = "en";
      if (!languages.includes(lang)) {
        if (languagesMatch[lang]) {
          lang = languagesMatch[lang];
        } else {
          lang = "en";
        }
      }
      await loadLanguage(lang);
      Object.assign(translations, loadedLanguages[lang]);
    },
    { immediate: true }
  );
};

export const useMKTi18n = () => {
  return {
    loadLanguage,
    mktt,
    initMkti18n,
    translations,
  };
};
