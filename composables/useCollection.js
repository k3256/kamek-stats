import { reactive, computed } from "vue";
import { useDb } from "./useDb";
import { sortItems } from "../helpers/ItemsHelper";
import {
  initStat,
  increaseStat,
  computeRegression,
  getGrade01,
  collectionGrades,
} from "../helpers/GradesHelper";
const { params, loadParams } = useParams();

const LEVEL_TICKETS = {
  1: 1,
  2: 2,
  3: 3,
  4: 5, //5,
  5: 7, //7,
  6: 10, //10,
  7: 15, //15,
  8: 23, //23,
};
const { db, getDriverById, getKartById, getGliderById, getItemById } = useDb();
const collection = reactive({
  loaded: false,
  drivers:
    [] /* Each entry contains id, l (level), p (points), c (cap), mii (boolean driver is mii (not saved)) */,
  karts: [],
  gliders: [],
  courses: reactive({}),
  utility: {
    driversById: reactive({}),
    kartsById: reactive({}),
    glidersById: reactive({}),
  }, // objects in collection usage
  wanted: {
    driversById: reactive({}),
    kartsById: reactive({}),
    glidersById: reactive({}),
  }, // objects missing in collection, count uncovered courses that are covered by these items
  newt3: {
    driversById: reactive({}),
    kartsById: reactive({}),
    glidersById: reactive({}),
  },
  grades: {
    driversById: reactive({}),
    kartsById: reactive({}),
    glidersById: reactive({}),
  },
});

const capsTable = {
  drivers: [
    {
      cap: 0,
      thresholds: [
        { r: 0, min: 400, max: 600, gap: 8 },
        { r: 1, min: 450, max: 675, gap: 9 },
        { r: 2, min: 500, max: 800, gap: 12 },
      ],
    },
    {
      cap: 1,
      thresholds: [
        { r: 0, min: 600, max: 648, gap: 8 },
        { r: 1, min: 675, max: 765, gap: 15 },
        { r: 2, min: 800, max: 980, gap: 30 },
      ],
    },
    {
      cap: 2,
      thresholds: [
        { r: 0, min: 648, max: 704, gap: 8 },
        { r: 1, min: 765, max: 870, gap: 15 },
        { r: 2, min: 980, max: 1190, gap: 30 },
      ],
    },
    {
      cap: 3,
      thresholds: [
        { r: 0, min: 704, max: 768, gap: 8 },
        { r: 1, min: 870, max: 990, gap: 15 },
        { r: 2, min: 1190, max: 1430, gap: 30 },
      ],
    },
    {
      cap: 4,
      thresholds: [
        { r: 0, min: 768, max: 816, gap: 8 },
        { r: 1, min: 990, max: 1080, gap: 15 },
        { r: 2, min: 1430, max: 1610, gap: 30 },
      ],
    },
  ],
  karts: [
    {
      cap: 0,
      thresholds: [
        { r: 0, min: 200, max: 300, gap: 4 },
        {
          r: 1,
          min: 220,
          max: 330,
          gap: [
            [280, 4],
            [330, 5],
          ],
        },
        { r: 2, min: 250, max: 400, gap: 6 },
      ],
    },
    {
      cap: 1,
      thresholds: [
        { r: 0, min: 300, max: 324, gap: 4 },
        { r: 1, min: 330, max: 366, gap: 6 },
        { r: 2, min: 400, max: 490, gap: 15 },
      ],
    },
    {
      cap: 2,
      thresholds: [
        { r: 0, min: 324, max: 352, gap: 4 },
        { r: 1, min: 366, max: 408, gap: 6 },
        { r: 2, min: 490, max: 595, gap: 15 },
      ],
    },
    {
      cap: 3,
      thresholds: [
        { r: 0, min: 352, max: 384, gap: 4 },
        { r: 1, min: 408, max: 456, gap: 6 },
        { r: 2, min: 595, max: 715, gap: 15 },
      ],
    },
    {
      cap: 4,
      thresholds: [
        { r: 0, min: 384, max: 408, gap: 4 },
        { r: 1, min: 456, max: 492, gap: 6 },
        { r: 2, min: 715, max: 805, gap: 15 },
      ],
    },
  ],
};
capsTable.gliders = capsTable.karts;

const pData = [
  {
    dbKey: "d",
    localKey: "drivers",
    localKeyById: "driversById",
    nbStat: "nbDrivers",
    bgr: "D",
  },
  {
    dbKey: "g",
    localKey: "gliders",
    localKeyById: "glidersById",
    nbStat: "nbGliders",
    bgr: "G",
  },
  {
    dbKey: "k",
    localKey: "karts",
    localKeyById: "kartsById",
    nbStat: "nbKarts",
    bgr: "K",
  },
];

export const useCollection = () => {
  const loadCollection = (fromJson) => {
    console.log("load collection reactivity");
    let ts = new Date().getTime();

    loadParams();

    if (!collection.loaded || fromJson) {
      let col;
      if (!fromJson) {
        let colstr = localStorage["mkt_collection"];
        if (colstr) {
          col = JSON.parse(colstr);
        }
      } else {
        col = fromJson;
      }
      if (!col) {
        col = collection;
      }
      //Object.assign(collection, col);

      for (let def of pData) {
        collection[def.localKey] = col[def.localKey];
        collection[def.localKeyById] = reactive({});
        for (let item of collection[def.localKey]) {
          collection[def.localKeyById][item.id] = { ...item };
          if (
            collection[def.localKeyById][item.id].mii == null &&
            def.localKey === "drivers"
          ) {
            let d = getDriverById(item.id);
            if (d.mii) {
              collection[def.localKeyById][item.id].mii = d.mii;
            }
          }
        }
      }

      if (fromJson) {
        saveCollection();
      }

      collection.loaded = true;
    }

    collection.nbMiiPoints = computed(() => {
      let ownedMiis = Object.keys(collection.driversById).filter(
        (k) => collection.driversById[k].mii
      );
      if (ownedMiis.length > 0) {
        return (ownedMiis.length - 1) * 10;
      } else {
        return 0;
      }
    });

    collection.stats = computed(() => {
      let res = {};
      for (let t of pData) {
        let ks = Object.keys(collection[t.localKeyById]) || [];
        res[t.localKey] = {
          totalOwned: (ks && ks.length) || 0,
          total: db[t.localKey].list.length,
        };
        res[t.localKey].percent = Math.floor(
          (res[t.localKey].totalOwned / res[t.localKey].total) * 100
        );
        res[t.localKey].rarity = [0, 1, 2].map((r) => {
          return {
            total: 0,
            totalOwned: 0,
          };
        });
        res[t.localKey].objects = {};
        res[t.localKey].levels = [1, 2, 3, 4, 5, 6, 7, 8].map((l) => ({
          level: l,
          total: 0,
          rarity: [0, 1, 2].map((r) => ({ r, total: 0 })),
        }));
        res[t.localKey].pertours = db.cups.list.map((c) => ({
          id: c.id,
          total: 0,
        }));
        for (let it of db[t.localKey].list) {
          let isInCollection = ks.includes("" + it.id);
          // by rarity
          res[t.localKey].rarity[it.r].total++;
          if (isInCollection) {
            res[t.localKey].rarity[it.r].totalOwned++;
          }
          // by object
          if (!res[t.localKey].objects[it.s]) {
            res[t.localKey].objects[it.s] = {
              total: 0,
              totalOwned: 0,
            };
          }
          res[t.localKey].objects[it.s].total++;
          if (isInCollection) {
            res[t.localKey].objects[it.s].totalOwned++;
          }

          // by level
          if (isInCollection) {
            let lvl = collection[t.localKeyById][it.id].l;
            res[t.localKey].levels[lvl - 1].total++;
            res[t.localKey].levels[lvl - 1].rarity[it.r].total++;

            // per tour
            res[t.localKey].pertours[it.t - 1].total++;
          }
        }
        res[t.localKey].objects = Object.keys(res[t.localKey].objects).map(
          (k) => ({ id: k, ...res[t.localKey].objects[k] })
        );
        res[t.localKey].objects.sort((a, b) => b.total - a.total);
        res[t.localKey].rarity = res[t.localKey].rarity.map((rs) => {
          return {
            ...rs,
            percent: Math.floor((rs.totalOwned / rs.total) * 100),
          };
        });
        if (res[t.localKey].totalOwned > 0) {
          res[t.localKey].meanlevel =
            (1.0 *
              res[t.localKey].levels.reduce(
                (acc, l) => acc + l.total * l.level,
                0
              )) /
            res[t.localKey].totalOwned;
        } else {
          res[t.localKey].meanlevel = 0;
        }
      }
      return res;
    });

    let comparer = sortItems(
      collection,
      (a) => a,
      (a) => a?.fromLevel
    );
    // load courses coverage
    for (let c of db.stats_courses) {
      collection.courses[c.id] = reactive({ unique: c.unique });
      for (let def of pData) {
        collection.courses[c.id][def.dbKey] = computed(() => {
          //console.log("compute", c.id);
          let res = [
            ...c.p[def.dbKey]
              .map((itemId) => collection[def.localKeyById][itemId])
              .filter(Boolean),
            ...c.p2[def.dbKey] // add secondary which have a fromLevel key
              .map((item) => {
                if (Array.isArray(item)) {
                  let col = collection[def.localKeyById][item[0]];
                  if (col) {
                    return reactive({
                      ...col,
                      fromLevel: item[1],
                    });
                  }
                }
              })
              .filter(Boolean),
          ];
          // sort best items for course
          res.sort(comparer);
          let top = [];
          if (res.length > 0) {
            let first = res[0];
            if (
              first.l > 0 &&
              (!first.fromLevel || first.fromLevel <= first.l)
            ) {
              let idx = res.findIndex((it) => comparer(first, it) != 0);
              if (idx > -1) {
                top = res.slice(0, idx).map((it) => it.id);
              } else {
                top = res.map((it) => it.id) || [];
              }
            } else {
              // first element is not usable (due to level)....
              top = [];
            }
          }
          return { list: res, top };
        });

        // compute stats for course
        // nbDrivers, nbKarts, nbGliders
        collection.courses[c.id][def.nbStat] = computed(() => {
          return (
            collection.courses[c.id][def.dbKey]?.list?.reduce(
              (acc, item) =>
                acc + (!item.fromLevel || item.fromLevel <= item.l ? 1 : 0),
              0
            ) || 0
          );
        });
      }
    }

    for (let def of pData) {
      // add computed for utility of not owned objects
      collection.wanted[def.localKeyById] = computed(() => {
        let res = {}; // key : id ; value : {nbCovered, nbNonUniqueCovered}
        const addId = (c, id) => {
          if (!res[id])
            res[id] = reactive({
              nbCovered: 0,
              nbNonUniqueCovered: 0,
              courses: [],
            });
          if (!collection.courses[c.id].unique) {
            res[id].nbNonUniqueCovered++;
          }
          res[id].nbCovered++;
          res[id].courses.push(c);
        };
        for (let c of db.stats_courses) {
          if (collection.courses[c.id][def.nbStat] === 0) {
            // missing item for this course, increment values for prefered
            c.p[def.dbKey].forEach((itemId) => addId(c, itemId));

            c.p2[def.dbKey] // add secondary which have a fromLevel key
              .forEach((item) => {
                if (Array.isArray(item)) {
                  addId(c, item[0]);
                }
              });
          }
        }
        let stats = {
          nbCovered: initStat(),
          nbNonUniqueCovered: initStat(),
        };
        stats.nbCovered.min = 0;
        stats.nbNonUniqueCovered.min = 0;

        for (let k in Object.keys(res)) {
          if (!res[k]) continue;
          increaseStat(stats, "nbCovered", res[k].nbCovered);
          increaseStat(stats, "nbNonUniqueCovered", res[k].nbNonUniqueCovered);
        }
        res.stats = stats;
        return res;
      });

      // compute utiliy of item
      collection.utility[def.localKeyById] = computed(() => {
        let res = {};
        for (let item of db[def.localKey].list) {
          let id = item.id;
          let itemcourses = item.preferred_courses.preferred;
          let nbUtile = 0,
            nbNonUniqueUtile = 0,
            tops = [];
          itemcourses?.forEach((c) => {
            let topitemslist = collection.courses[c.id][def.dbKey]?.top;
            let isUtile = topitemslist.includes(item.id);
            //console.log(def.dbKey, c.id, topitemslist, isUtile, item.id);
            nbUtile += isUtile ? 1 : 0;
            nbNonUniqueUtile +=
              !collection.courses[c.id].unique && isUtile ? 1 : 0;
            if (!isUtile) {
              let fd = { l: 1, p: 0, c: 0 };
              if (topitemslist.length > 0) {
                fd = collection[def.localKeyById][topitemslist[0]];
              }
              tops.push({
                //c: c.n,
                //items: topitemslist,
                level: Math.max(c.fromLevel || 0, fd.l || 0),
                points: fd.p,
              });
            }
          });
          tops.sort((a, b) => {
            return b.level === a.level
              ? a.points - b.points
              : a.level - b.level;
          });
          let itemcollec = collection[def.localKeyById][id];
          let topbyLevel = {},
            currentcumul = 0; // {lvl, points: [{points, nbAdd}]}
          tops.forEach((t, idxt) => {
            let p = 0;
            if (!itemcollec || itemcollec.p < t.points) {
              p = t.points;
            }
            // check if required points exceed max points possible for item's rarity
            let caps = capsTable[def.localKey];
            let lastcapthreshold = caps[caps.length - 1].thresholds;
            let forRarity = lastcapthreshold.find((th) => th.r === item.r);
            if (p > forRarity.max) {
              p = 0;
              t.level++;
            }
            if (!topbyLevel[t.level]) {
              topbyLevel[t.level] = {
                level: t.level,
                points: {},
                cumullevel: currentcumul,
              };
            }
            if (!topbyLevel[t.level].points[p]) {
              topbyLevel[t.level].points[p] = { points: p, nbadd: 1 };
            } else {
              topbyLevel[t.level].points[p].nbadd++;
            }
            topbyLevel[t.level].cumullevel++;
            currentcumul++;
          });

          res[id] = {
            nbUtile,
            nbNonUniqueUtile,
            tops: Object.keys(topbyLevel).map((k) => {
              return {
                ...topbyLevel[k],
                points: Object.keys(topbyLevel[k].points).map((kp) => {
                  return topbyLevel[k].points[kp];
                }),
              };
            }),
          };
        }
        let stats = {
          nbUtile: initStat(),
          nbNonUniqueUtile: initStat(),
        };
        stats.nbUtile.min = 0;
        stats.nbNonUniqueUtile.min = 0;

        for (let k in Object.keys(res)) {
          if (!res[k]) continue;
          increaseStat(stats, "nbUtile", res[k].nbUtile);
          increaseStat(stats, "nbNonUniqueUtile", res[k].nbNonUniqueUtile);
        }
        res.stats = stats;
        return res;
      });

      // compute utiliy of item
      collection.newt3[def.localKeyById] = computed(() => {
        let res = {};
        for (let item of db[def.localKey].list) {
          let id = item.id;
          let itemcoursesids = item.preferred_courses.preferred.map(
            (c) => c.id
          );
          let alreadyPassed = new Set();
          res[id] = db.cups.list.map((c, idx) => {
            if (item.t <= c.id) {
              let allcs = c.c.flatMap((cc) => cc.c);
              // for each courses of cup, check if it's t3 of item and not already passed
              let nb = 0;
              allcs.forEach((cid) => {
                if (itemcoursesids.includes(cid) && !alreadyPassed.has(cid)) {
                  nb++;
                  alreadyPassed.add(cid);
                }
              });
              return nb;
            } else {
              return 0;
            }
          });
        }
        return res;
      });

      // compute collectin grades
      collection.grades[def.localKeyById] = computed(() => {
        let res = {},
          stats = {
            ticketPrice10: initStat(),
            ticketPrice50: initStat(),
          };
        for (let item of db[def.localKey].list) {
          let id = item.id;
          res[id] = {};
          // compute effort10 grade
          let tops = collection.utility[def.localKeyById][id].tops;
          let top10 = tops.find((t) => t.cumullevel >= 10);
          if (!top10) top10 = tops[tops.length - 1] || {};
          let curLevel =
            (collection[def.localKeyById] &&
              collection[def.localKeyById][id] &&
              collection[def.localKeyById][id].l) ||
            0;
          res[id].more10 = {
            level: top10.level || 8,
            nbTickets:
              LEVEL_TICKETS[top10.level || 8] - (LEVEL_TICKETS[curLevel] || 0),
            cumul: top10.cumullevel,
          };
          res[id].more10.price = Math.min(
            1,
            res[id].more10.nbTickets / res[id].more10.cumul
          );
          increaseStat(stats, "ticketPrice10", res[id].more10.price);

          // compute effort50 grade
          let nbUtile = collection.utility[def.localKeyById][id].nbUtile;
          let totalUtile = item.preferred_courses.preferred.length;
          let nb50 = Math.ceil((totalUtile - nbUtile) / 2);
          let top50 = tops.find((t) => t.cumullevel >= nb50);
          if (!top50) top50 = tops[tops.length - 1] || {};
          res[id].more50p = {
            level: top50.level || 8,
            nbTickets:
              LEVEL_TICKETS[top50.level || 8] - (LEVEL_TICKETS[curLevel] || 0),
            cumul: top50.cumullevel,
          };
          res[id].more50p.price = Math.min(
            1,
            res[id].more50p.nbTickets / res[id].more50p.cumul
          );
          increaseStat(stats, "ticketPrice50", res[id].more50p.price);
        }
        for (let item of db[def.localKey].list) {
          let id = item.id;
          res[id].grades = {};
          res[id].grades.effort10 = getGrade01(
            1 - computeRegression(res[id].more10.price, stats.ticketPrice10)
          );
          res[id].grades.effort50p = getGrade01(
            1 - computeRegression(res[id].more50p.price, stats.ticketPrice50)
          );
          res[id].grades.wanted = collection.wanted[def.localKeyById][id]
            ? getGrade01(
                computeRegression(
                  collection.wanted[def.localKeyById][id].nbCovered,
                  collection.wanted[def.localKeyById].stats.nbCovered
                )
              )
            : 0;
          res[id].grades.wantedhcity = collection.wanted[def.localKeyById][id]
            ? getGrade01(
                computeRegression(
                  collection.wanted[def.localKeyById][id].nbNonUniqueCovered,
                  collection.wanted[def.localKeyById].stats.nbNonUniqueCovered
                )
              )
            : 0;
          res[id].grades.utility = collection.utility[def.localKeyById][id]
            ? getGrade01(
                computeRegression(
                  collection.utility[def.localKeyById][id].nbUtile,
                  collection.utility[def.localKeyById].stats.nbUtile
                )
              )
            : 0;
          res[id].grades.utilityhcity = collection.utility[def.localKeyById][id]
            ? getGrade01(
                computeRegression(
                  collection.utility[def.localKeyById][id].nbNonUniqueUtile,
                  collection.utility[def.localKeyById].stats.nbNonUniqueUtile
                )
              )
            : 0;
          let grade = 0,
            bonus = 0,
            weight = 0;
          Object.keys(res[id].grades).forEach((gkey) => {
            let w = params.grades.collection[gkey] || 0;
            let defg = collectionGrades.find((c) => c.name === gkey);
            if (defg?.bonus) {
              bonus += (res[id].grades[gkey] / 100) * w;
            } else {
              grade += res[id].grades[gkey] * w;
              weight += w;
            }
          });
          if (weight > 0) {
            grade /= weight;
          }
          res[id].grades.total = Number(grade + bonus).toFixed(1);
        }
        return res;
      });
    }

    console.log("-> loaded in", new Date().getTime() - ts, "ms");
  };
  const removeItem = ({ type, id }) => {
    let pt = pData.find((d) => d.localKey === type);
    let it = collection[pt.localKeyById][id];

    if (it) {
      delete collection[pt.localKeyById][id];
    }
    saveCollection();
  };
  const getCurrentThreshold = (type, id, cap = 0) => {
    let caps = capsTable[type];
    if (caps) {
      let item = getItemById(type, id);
      if (item) {
        let capt = caps.find((t) => t.cap === cap);
        if (capt) {
          let captr = capt.thresholds.find((captt) => captt.r === item.r);
          if (captr) {
            return captr;
          }
        }
      }
    }
    return null;
  };
  const increasePoints = (type, id) => {
    let pt = pData.find((d) => d.localKey === type);
    let it = collection[pt.localKeyById][id];

    if (it) {
      let curTh = getCurrentThreshold(type, id, it.c);

      if (curTh && it.p < curTh.max) {
        if (Array.isArray(curTh.gap)) {
          let current = curTh.gap.find((arr) => arr[0] > it.p);
          it.p += current[1];
        } else {
          it.p += curTh.gap;
        }
      }
      saveCollection();
    }
  };
  const decreasePoints = (type, id) => {
    let pt = pData.find((d) => d.localKey === type);
    let it = collection[pt.localKeyById][id];

    if (it) {
      let curTh = getCurrentThreshold(type, id, it.c);

      if (curTh && it.p > curTh.min) {
        if (Array.isArray(curTh.gap)) {
          let current = curTh.gap.find((arr) => arr[0] >= it.p);
          it.p -= current[1];
        } else {
          it.p -= curTh.gap;
        }
      }
      saveCollection();
    }
  };
  const getDefaultPoints = (type, id) => {
    let pt = pData.find((d) => d.localKey === type);
    let it = collection[pt.localKeyById][id];

    let curTh = getCurrentThreshold(type, id, it?.c);

    return curTh.max;
  };
  const updateItem = ({ type, id, level, points, cap }) => {
    let pt = pData.find((d) => d.localKey === type);
    let it = collection[pt.localKeyById][id];

    if (it) {
      if (!it.l) level = 1;
      if (!it.p) {
        points = getDefaultPoints(type, id);
      }
      if (level != null) {
        it.l = level;
      }
      if (points != null) {
        it.p = points;
      }
      if (cap != null) {
        it.c = cap;
        it.p = getDefaultPoints(type, id);
      }
      // this is "forcing" reactivity of computed on object update. In some cases it was malfunctionning :
      // when updating => everythig ok (list sorted, update reorders the list)
      // navigates until an error occurs (ScrollPanel is generating errors when unmounted, some events keeps firing and dom is removed)
      // after that, without this "forcing line", lists are not reordering when item is updated => meaning computed is not re evaluated.
      // this line is fixing the misbehaviour
      collection[pt.localKeyById][id] = { ...it };
    } else {
      if (!level) level = 1;
      if (!points) {
        points = getDefaultPoints(type, id);
      }
      let nit = { id, l: level, p: points, c: 0 };
      if (type === "drivers") {
        let d = getDriverById(id);
        nit.mii = d.mii;
      }
      collection[pt.localKeyById][id] = nit;
    }
    saveCollection();
  };
  const getCollection = () => {
    return {
      drivers: Object.keys(collection.driversById).map((k) => {
        return {
          id: collection.driversById[k].id,
          l: collection.driversById[k].l,
          p: collection.driversById[k].p,
          c: collection.driversById[k].c,
        };
      }),
      karts: Object.keys(collection.kartsById).map((k) => {
        return {
          id: collection.kartsById[k].id,
          l: collection.kartsById[k].l,
          p: collection.kartsById[k].p,
          c: collection.kartsById[k].c,
        };
      }),
      gliders: Object.keys(collection.glidersById).map((k) => {
        return {
          id: collection.glidersById[k].id,
          l: collection.glidersById[k].l,
          p: collection.glidersById[k].p,
          c: collection.glidersById[k].c,
        };
      }),
    };
  };
  const getBGR = (item, col, type) => {
    return (
      item.n
        .toUpperCase()
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "")
        .replace(/(?<!\s)\(/g, " (")
        .trim() +
      "\t" +
      type +
      "\t" +
      ((col && col.l) || 0) +
      "\t" +
      ((col && col.c) || 0)
    );
  };
  const getCollectionBGR = () => {
    let res = [];
    for (let def of pData) {
      res.push(
        ...db[def.localKey].list.map((it) => {
          return getBGR(it, collection[def.localKeyById][it.id], def.bgr);
        })
      );
    }
    return res.join("\n");
  };

  const saveCollection = () => {
    localStorage["mkt_collection"] = JSON.stringify(getCollection());
    //console.log("save", JSON.parse(JSON.stringify(getCollection())));
  };
  return {
    loadCollection,
    collection,
    capsTable,
    updateItem,
    removeItem,
    decreasePoints,
    increasePoints,
    getCollection,
    getCollectionBGR,
  };
};
