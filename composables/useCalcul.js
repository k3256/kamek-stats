import actionData from "../assets/tad.json"; // assert { type: "json" };
import { useCollection } from "./useCollection";

export const useCalcul = () => {
  const { collection } = useCollection();
  const calculate = (courseId, driver, kart, glider) => {
    let data = actionData[courseId];
    let res = 0;
    if (data) {
      let bonusPoints =
        bonus("drivers", driver) +
        bonus("karts", kart) +
        bonus("gliders", glider);
      console.log(bonusPoints);
    }
    return res;
  };
  const bonus = (type, item) => {
    if (item.collect) {
      let p = item.collect?.p;
      if (type === "drivers" && item.collect?.mii) {
        p += collection.nbMiiPoints;
      }
      console.log(type, p, item.collect.l);
      return (p * (item.collect.l - 1) * 150) / 30000;
    }
    return 0;
  };
  return {
    actionData,
    calculate,
    bonus,
  };
};
