import { reactive } from "vue";

const params = reactive({
  importance: 39,
  grades: {
    general: {
      favorites: 2,
      favorites_nonunique: 2,
      rankedCups: 4,
      cups: 2,
      oldness: 6,
      city: 2,
      skill: 2,
    },
    collection: {
      /*concurrency: 4,*/
      effort10: 1,
      effort50p: 1,
      wanted: 10, // bonus
      wantedhcity: 50, // bonus
      utility: 5, // bonus
      utilityhcity: 15, // bonus
    },
  },
  skills: {
    drivers: [
      { id: 1, name: "Boomerang Flower", grade: 95 },
      { id: 2, name: "Bubble", grade: 50 },
      { id: 3, name: "Triple Green Shells", grade: 50 },
      { id: 4, name: "Double Bob-ombs", grade: 60 },
      { id: 5, name: "Dash Ring", grade: 75 },
      { id: 6, name: "Giant Banana", grade: 90 },
      { id: 7, name: "Heart", grade: 75 },
      { id: 8, name: "Fire Flower", grade: 70 },
      { id: 9, name: "Yoshi's Egg", grade: 70 },
      { id: 10, name: "Triple Mushrooms", grade: 65 },
      { id: 11, name: "Ice Flower", grade: 60 },
      { id: 12, name: "Bowser's Shell", grade: 75 },
      { id: 13, name: "Banana Barrels", grade: 55 },
      { id: 14, name: "Lucky Seven", grade: 80 },
      { id: 15, name: "Bob-omb Cannon", grade: 60 },
      { id: 16, name: "Birdo's Egg", grade: 70 },
      { id: 17, name: "Hammer", grade: 70 },
      { id: 18, name: "Mushroom Cannon", grade: 60 },
      { id: 19, name: "Coin Box", grade: 100 },
      { id: 20, name: "Super Bell", grade: 65 },
      { id: 21, name: "Triple Bananas", grade: 65 },
      { id: 22, name: "Super Leaf", grade: 70 },
      { id: 23, name: "Giga Bob-omb", grade: 65 },
      { id: 24, name: "Capsule", grade: 60 },
    ],
    karts: [
      { id: 0, name: "Dash Panel Plus", grade: 90 },
      { id: 1, name: "Jump Boost Plus", grade: 100 },
      { id: 2, name: "Mini-Turbo Plus", grade: 100 },
      { id: 3, name: "Slipstream Plus", grade: 75 },
      { id: 4, name: "Rocket Start Plus", grade: 70 },
    ],
    gliders: [
      { id: 1, name: "Mushroom", grade: 90 },
      { id: 2, name: "Blooper", grade: 80 },
      { id: 3, name: "Green Shell", grade: 50 },
      { id: 4, name: "Banana", grade: 60 },
      { id: 5, name: "Mega Mushroom", grade: 50 },
      { id: 6, name: "Red Shell", grade: 80 },
      { id: 7, name: "Lightning", grade: 60 },
      { id: 8, name: "Super Horn", grade: 60 },
      { id: 9, name: "Coin", grade: 100 },
      { id: 10, name: "Bob-omb", grade: 50 },
      { id: 11, name: "Bullet Bill", grade: 60 },
    ],
  },
});

export const useParams = () => {
  const loadParams = (fromJson) => {
    console.log("load params");
    try {
      if (!params.loaded && window && window["localStorage"]) {
        let pstr = localStorage["mkt_params"];
        if (pstr) {
          let stored = JSON.parse(pstr);
          Object.assign(params.grades.general, stored.grades.general);
          Object.assign(params.grades.collection, stored.grades.collection);
          Object.assign(params.skills.drivers, stored.skills.drivers);
          Object.assign(params.skills.karts, stored.skills.karts);
          Object.assign(params.skills.gliders, stored.skills.gliders);
        }
        params.loaded = true;
      }
    } catch (e) {
      // not in browser
    }
  };
  const saveParams = () => {
    localStorage["mkt_params"] = JSON.stringify(params);
  };
  return {
    params,
    loadParams,
    saveParams,
  };
};
