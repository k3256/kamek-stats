import grades from "../assets/grades.json"; // assert { type: "json" };

export const useGrades = () => {
  return {
    grades,
  };
};
