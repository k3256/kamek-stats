import fs from "fs";
import { useDb } from "../composables/useDb.js";

import { createRequire } from "node:module";
const require = createRequire(import.meta.url);
const dbfull = require("../assets/db.json");

const matchType = (type) => {
  if (type === "drivers") return "d";
  if (type === "karts") return "k";
  if (type === "gliders") return "g";
};
const getPartialDb = (lasttour) => {
  let res = JSON.parse(JSON.stringify(dbfull));
  res.cups.list = res.cups.list.slice(0, lasttour);
  console.log(res.cups.list.length);
  // list of existing courses
  let courses = [
    ...new Set(res.cups.list.flatMap((c) => c.c).flatMap((c) => c.c)),
  ];
  // keep only courses for first tours
  res.preferred.list = res.preferred.list.filter((c) => courses.includes(c.id));
  let existList = {};
  [
    // filter drivers and remove unknown from courses
    "drivers",
    "karts",
    "gliders",
  ].forEach((type) => {
    res[type].list = res[type].list.filter((d) => d.t <= lasttour);
    existList[type] = res[type].list.map((it) => it.id);
  });
  res.preferred.list = res.preferred.list.map((c) => {
    let rc = { ...c };
    ["drivers", "karts", "gliders"].forEach((type) => {
      let st = matchType(type);
      rc.p[st] = rc.p[st].filter((id) => existList[type].includes(id));
      rc.p2[st] = rc.p2[st].filter((id) =>
        Array.isArray(id)
          ? existList[type].includes(id[0])
          : existList[type].includes(id)
      );
    });
    return rc;
  });
  return res;
};

const sortGrade = (a, b) => {
  return Number(b.grades.value.total) - Number(a.grades.value.total);
};
const getGrades = (tour) => {
  let dbtour = getPartialDb(tour);
  let { loadDbGrades, db } = useDb(dbtour, true);
  loadDbGrades();
  let res = { tour };
  ["drivers", "karts", "gliders"].forEach((type) => {
    res[type] = {};
    db[type].list.sort(sortGrade);
    db[type].list.forEach((it, idx) => {
      res[type][it.id] = { g: Number(it.grades.value.total), p: idx + 1 };
    });
  });
  return res;
};

const computeAllGrades = () => {
  let res = { drivers: {}, karts: {}, gliders: {}, tours: {} };
  for (let i = 0; i < dbfull.cups.list.length; i++) {
    console.log("compute grades for tour", dbfull.cups.list[i].n);
    let grades = getGrades(i + 1);
    res.tours[i] = {};
    ["drivers", "karts", "gliders"].forEach((type) => {
      res.tours[i][type] = Object.keys(grades[type]).length;
      for (let id of Object.keys(grades[type])) {
        if (res[type][id]) {
          res[type][id].g.push(grades[type][id].g);
          res[type][id].p.push(grades[type][id].p);
        } else {
          res[type][id] = {
            g: [grades[type][id].g],
            p: [grades[type][id].p],
            t: i + 1,
          };
        }
      }
    });
  }
  fs.writeFileSync("./assets/grades.json", JSON.stringify(res));
  console.log("done");
};
computeAllGrades();
//let res = getGrades(90);
/*
fs.writeFileSync(
  "./statsgen/gen/1.json",
  JSON.stringify(getPartialDb(1), null, 2)
);
*/
